**Sorted in no particular order in each category**

# High Priority

* Households
    - Integrate import/export function front end side.

* Items/Packages
    - ~~File import/export per items.~~ 
    - ~~mayor approval~~ (may need to re-code)

* Bugs and Issues
    - Fix display proper reception status in operations.
    - Fix proper display of reception proof
    - ~~Fix issue being unable to add households on front end user side.~~
    - ~~Fix Login page (front end) UI.~~
    - ~~Can't delete in Firestore.~~
    - ~~Can't edit in Firestore.~~
    - ~~Can't edit in db.~~
    - ~~Resident inline not showing in household add form.~~
    - ~~Only one foreign object is being saved in Firestore.~~
    - ~~Does not accept raw PK value when updating objects. ~~
    - ~~Fix data POST issue in SMS.~~
    - ~~Fix number of households in a barangay display.~~
    - ~~Barangay - Model relationship bug.~~
    - ~~Household number_of_members not displaying properly.~~

* Inventory
    - Fix/implement item quantity decrement after assigning to households/barangays.
    - Display proper data info.

    - ~~data entry.~~
    - ~~donor assigned per item.~~
    - ~~donation source.~~

* Admin 
    - Improve data import/export formatting.

    - ~~UI color change. lighter.~~
    - ~~address input.auto fill address info based on user.~~
    - ~~Access scope.~~
    
* Operations module features.
    - Distribution reports.
    
    - ~~Distribution tracker.~~
    - ~~Distribution/delivery assignment.~~

* ~~Fix admin UI layout for add household.~~
    - ~~add residents inline form.~~
    - ~~display households in admin. (issue might be in the save/delete override functions).~~

* ~~SMS module.~~
    - ~~look for python API for SMS messaging.~~
        ~~- Look into Twilio Django 3 API.~~

* ~~Setup basic UI.~~
    - ~~UI for households form.~~
    - ~~proper routing for features in nav bar.~~
    - ~~fix issue : unable to route to households app.~~


* ~~Create Models.~~
    - ~~Created successful POST to firestore.~~
    - ~~Design data structure for households>families>residents~~
    - ~~Override django user models. integrate to firestore.~~
    - ~~Barangay officials model.~~
    - ~~"Departments" model.~~
    - ~~Change residents model to "members".~~
    - ~~Household main identifier change to house number.~~


    
* ~~Define apps needed.~~

~~Setup test admin with test models~~

# Neutral Priority

~~* User login module.~~

* Check how to override Django forms UI.

~~Django firebase setup.~~

~~Django bootstrap 5 setup.~~

~~Merge bootstrap 5 and master branch.~~

# Low Priority

* UI fixes.

* Improve Django-admin UI design.

* Improve Django form UI.

* Improve UI design.

* Paypal sandbox for donations.