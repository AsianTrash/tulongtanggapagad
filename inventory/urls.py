from django.urls import path

from . import views #from *this* directory import views.py

app_name = 'inventory'
urlpatterns =   [
    path('relief_inventory/', views.IndexView.as_view(), name='index'),
    path('received_packages/', views.ReceivedPackagesView.as_view(), name='received_packages'),
    path('received_packages/<int:reliefpackage_id>/', views.PackageItemsView.as_view(), name='package_items'),
    path('received_packages/<int:pk>/reception_verification/', views.ReceptionUpdateView.as_view(), name='reception'),
]