from django.contrib import admin
from managementsystem.models import *
from django.conf.urls import url
from import_export import resources
from import_export.admin import ImportExportModelAdmin

class DonorInline(admin.TabularInline):
    model = Donor
    extra = 0

class ItemInline(admin.TabularInline):
    model = Item
    extra = 3

class ReliefPackageAdmin(admin.ModelAdmin):
    
    fieldsets = [
        (None, {'fields': ['name']}),
        (None, {'fields': ['date_received']}),
        (None, {'fields': ['destination']}),
        (None, {'fields': ['is_donated']}),
        (None, {'fields': ['relief_items']}),
        (None, {'fields': ['status']}),
        (None, {'fields': ['date_of_approval']}),
        (None, {'fields': ['approval_instruction']}),
    ]

    readonly_fields = ['date_received', 'status', 'date_of_approval', 'approval_instruction', 'reception_status']
    inlines = [DonorInline]
    search_fields = ['name']
    list_display = ('name', 'destination', 'is_donated', 'date_received', 'status', 'reception_status')

    def get_readonly_fields(self, request, obj=None):
        if request.user.groups.filter(name='Mayor').exists():
            return ['date_received', 'date_of_approval']
        return self.readonly_fields

class ItemResource(resources.ModelResource):
    class Meta:
        model = Item
        fields = ('name', 'quantity','fund_source')
        export_order = ('name', 'quantity', 'fund_source')
        # skip_unchanged = True
        # report_skipped = True
        exclude = ('id',)
        import_id_fields = ('name', 'quantity', 'fund_source')

class ItemAdmin(ImportExportModelAdmin):
    list_display = ('name', 'quantity', 'fund_source', 'date_restocked')
    resource_class = ItemResource

admin.site.register(ReliefPackage, ReliefPackageAdmin)
admin.site.register(Item, ItemAdmin)