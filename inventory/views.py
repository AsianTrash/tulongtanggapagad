from django.shortcuts import render
from django.views import generic
from django.urls import reverse
from managementsystem.models import *
from managementsystem.forms import *
from django.views.generic.edit import CreateView, UpdateView

# Create your views here.

class IndexView(generic.ListView):         
    template_name = 'inventory/index.html'
    model = Item

class ReceivedPackagesView(generic.ListView):         
    template_name = 'inventory/received_packages.html'
    model = ReliefPackage

class PackageItemsView(generic.ListView):         
    template_name = 'inventory/received_package_items.html'
    model = ReliefPackage
    
    def get_queryset(self):
        return ReliefPackage.objects.filter(id = self.kwargs['reliefpackage_id'])

class ReceptionUpdateView(UpdateView):
    model = ReliefPackage
    fields = ['reception_status']
    template_name = 'inventory/reception.html'

    def get_success_url(self):
        return reverse('inventory:received_packages')