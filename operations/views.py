from django.views import generic
from django.urls import reverse
from managementsystem.models import *
from managementsystem.forms import *
from django.views.generic.edit import CreateView

class IndexView(generic.ListView): # loads up distribution tracker page
    template_name = 'operations/index.html'
    model = Barangay

class BarangayHouseholdsView(generic.ListView): # displays list of households per barangay in the list
    template_name = 'operations/barangay_households.html'
    model = Household

    def get_queryset(self): # captures the id/variable passed on to the url
        barangay = Barangay.objects.get(id = self.kwargs['barangay_id'])
        return Household.objects.filter(barangay = barangay)