from django.contrib import admin
from managementsystem.models import *
from django.conf.urls import url
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from jet.admin import CompactInline

# Monitoring Report

class MonitoringReportResource(resources.ModelResource):
    
    class Meta:
        model = MonitoringReport
        fields = ('municipality', 'household_affected', 'persons_affected', 
                    'household_served', 'persons_served', 'relief_pack_quantity', 'cost_spent', 'remarks', 'pub_date')
        export_order = ('municipality', 'household_affected', 'persons_affected', 
                    'household_served', 'persons_served', 'relief_pack_quantity', 'cost_spent', 'remarks', 'pub_date')
        # exclude = ('id',)
        # import_id_fields = ('municipality', 'name')

class MonitoringReportAdmin(ImportExportModelAdmin):
    fieldsets = [
        (None, {'fields': ['municipality']}),
        (None, {'fields': ['remarks']}),
    ]
    list_display = ('municipality', 'household_affected', 'persons_affected', 
                    'household_served', 'persons_served', 'relief_pack_quantity', 'cost_spent', 'remarks', 'pub_date')
    
    readonly_fields = ['municipality', 'household_affected', 'persons_affected','household_served', 'persons_served', 'pub_date']
    
    resource_class = MonitoringReportResource

# Tracking Report

class TrackingReportResource(resources.ModelResource):
    
    class Meta:
        model = TrackingReport
        fields = ('barangay', 'registered_households', 'received_assistance', 
                    'completion_rate', 'remarks', 'pub_date')
        export_order = ('barangay', 'registered_households', 'received_assistance', 
                    'completion_rate', 'remarks', 'pub_date')
        exclude = ('id',)
        # import_id_fields = ('municipality', 'name')

class TrackingReportAdmin(ImportExportModelAdmin):
    fieldsets = [
        (None, {'fields': ['barangay']}),
        (None, {'fields': ['remarks']}),
    ]
    list_display = ('barangay', 'registered_households', 'received_assistance', 
                    'completion_rate', 'remarks', 'pub_date')
    
    readonly_fields = ['registered_households', 'received_assistance','completion_rate', 'pub_date']
    
    extra = 3
    resource_class = TrackingReportResource

# Risk Assessment Form

class RiskAssessmentInfoInline(admin.StackedInline):
    model = RiskAssessmentInfo
    extra = 0
    
    fieldsets = [
        (None, {'fields': ['risk']}),   
        (None, {'fields': ['livelihood']}),
        (None, {'fields': ['hazard_level']}),
        (None, {'fields': ['actions_required']}),
        (None, {'fields': ['action_taker']}),   
        (None, {'fields': ['action_date']}),
        (None, {'fields': ['response_strategy']}),
        (None, {'fields': ['evaluation_results']}),
    ]

class RiskAssessmentResource(resources.ModelResource):
    
    class Meta:
        model = RiskAssessment
        fields = ('workplace_location', 'assessment_conductor', 'assessment_date', 
                    'reviewed_by',)
        export_order = ('workplace_location', 'assessment_conductor', 'assessment_date', 
                    'reviewed_by',)
        exclude = ('id',)

        import_id_fields = ('workplace_location', 'assessment_conductor', 'assessment_date', 
                    'reviewed_by',)

class RiskAssessmentAdmin(ImportExportModelAdmin):
    fieldsets = [
        (None, {'fields': ['workplace_location']}),
        (None, {'fields': ['assessment_conductor']}),
        (None, {'fields': ['assessment_date']}),
        (None, {'fields': ['reviewed_by']}),
    ]

    inlines = [RiskAssessmentInfoInline]
    list_display = ('workplace_location', 'assessment_conductor', 'assessment_date', 
                    'reviewed_by',)
    extra = 1
    resource_class = RiskAssessmentResource

# Budget Request Form

class BudgetRequestInfoInline(admin.TabularInline):
    model = BudgetRequestInfo
    extra = 0
    
    fieldsets = [
        (None, {'fields': ['budget_category']}),   
        (None, {'fields': ['cost']}),
        (None, {'fields': ['required_documentation']}),
    ]

class BudgetRequestResource(resources.ModelResource):
    
    class Meta:
        model = BudgetRequest
        fields = ('requestor', 'email', 'date_requested', 
                    'request_period_start', 'request_period_end')
        export_order = ('requestor', 'email', 'date_requested', 
                    'request_period_start', 'request_period_end')
        exclude = ('id',)

        import_id_fields = ('requestor', 'email', 'date_requested', 
                    'request_period_start', 'request_period_end')

class BudgetRequestAdmin(ImportExportModelAdmin):
    fieldsets = [
        (None, {'fields': ['requestor']}),
        (None, {'fields': ['email']}),
        (None, {'fields': ['request_period_start']}),
        (None, {'fields': ['request_period_end']}),
        (None, {'fields': ['households_file']}),
    ]

    readonly_fields = ['date_requested']
    inlines = [BudgetRequestInfoInline]
    list_display = ('requestor', 'email', 'date_requested', 
                    'request_period_start', 'request_period_end')
    extra = 1
    resource_class = BudgetRequestResource

# DILG Advisory Form

class DILGAdvisoryResource(resources.ModelResource):
    
    class Meta:
        model = DILGAdvisory
        fields = ('subject', 'date', 'advisory_content')
        export_order = ('subject', 'date', 'advisory_content')
        exclude = ('id',)

        import_id_fields = ('subject', 'date', 'advisory_content')

class DILGAdvisoryAdmin(ImportExportModelAdmin):
    fieldsets = [
        (None, {'fields': ['subject']}),
        (None, {'fields': ['date']}),
        (None, {'fields': ['advisory_content']}),
        (None, {'fields': ['document']}),
    ]

    list_display = ('subject', 'date', 'advisory_content_html', 'document')
    resource_class = DILGAdvisoryResource

# Supply Request Form

class SupplyRequestInfoInline(admin.TabularInline):
    model = SupplyRequestInfo
    extra = 0
    
    fieldsets = [
        (None, {'fields': ['item_name']}),   
        (None, {'fields': ['item_description']}),
        (None, {'fields': ['item_category']}),
        (None, {'fields': ['quantity']}),
        (None, {'fields': ['cost']}),
        (None, {'fields': ['sub_total']}),
    ]

    readonly_fields = ['sub_total']
                    
class SupplyRequestResource(resources.ModelResource):
    
    class Meta:
        model = SupplyRequest
        fields = ('organization', 'date_form_completed', 'date_needed_by', 
                    'contact_person', 'contact_number')
        export_order = ('organization', 'date_form_completed', 'date_needed_by', 
                    'contact_person', 'contact_number')
        exclude = ('id',)

        import_id_fields = ('organization', 'date_form_completed', 'date_needed_by', 
                    'contact_person', 'contact_number')

class SupplyRequestAdmin(ImportExportModelAdmin):
    fieldsets = [
        (None, {'fields': ['organization']}),
        (None, {'fields': ['date_form_completed']}),
        (None, {'fields': ['date_needed_by']}),
        (None, {'fields': ['contact_person']}),
        (None, {'fields': ['contact_number']}),
    ]

    inlines = [SupplyRequestInfoInline]
    list_display = ('organization', 'contact_person', 'contact_number', 
                    'date_form_completed', 'date_needed_by')
    extra = 1
    resource_class = SupplyRequestResource

class ApprovalDocumentAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['document_title']}),
        (None, {'fields': ['approval_file']}),
        (None, {'fields': ['status']}),
    ]

    readonly_fields = ['approval_date']
    list_display = ('document_title', 'approval_file', 'status', 
                    'approval_date')

class FundAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['amount']}),
        (None, {'fields': ['source']}),
        (None, {'fields': ['date_released']}),
    ]

    readonly_fields = ['date_received']
    list_display = ('amount', 'source', 'date_released', 
                    'date_received')

# Accomplishment Form

class ARObjectiveInline(admin.TabularInline):
    model = ARObjective
    extra = 1
    
    fieldsets = [
        (None, {'fields': ['objective']}),
    ]

class ARHighlightsInline(admin.TabularInline):
    model = ARHighlight
    extra = 1
    
    fieldsets = [
        (None, {'fields': ['highlight']}),
    ]

class AccomplishmentReportAdmin(ImportExportModelAdmin):
    fieldsets = [
        (None, {'fields': ['title']}),
        (None, {'fields': ['introduction']}),
        (None, {'fields': ['report_date']}),
    ]

    inlines = [ARObjectiveInline, ARHighlightsInline]
    list_display = ('title', 'report_date')
    readonly_fields = ['report_date']
    extra = 1

admin.site.register(MonitoringReport, MonitoringReportAdmin)
admin.site.register(TrackingReport, TrackingReportAdmin)
admin.site.register(RiskAssessment, RiskAssessmentAdmin)
admin.site.register(BudgetRequest, BudgetRequestAdmin)
admin.site.register(DILGAdvisory, DILGAdvisoryAdmin)
admin.site.register(SupplyRequest, SupplyRequestAdmin)
admin.site.register(ApprovalDocument, ApprovalDocumentAdmin)
admin.site.register(Fund, FundAdmin)
admin.site.register(AccomplishmentReport, AccomplishmentReportAdmin)