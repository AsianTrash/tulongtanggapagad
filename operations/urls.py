from django.urls import path

from . import views #from *this* directory import views.py

app_name = 'operations'
urlpatterns =   [
    path('distribution_tracker/', views.IndexView.as_view(), name='index'),  
    path('<int:barangay_id>/barangay_households/', views.BarangayHouseholdsView.as_view(), name='barangay_households'),   
]