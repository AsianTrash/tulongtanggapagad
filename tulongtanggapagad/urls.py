from django.contrib import admin
from django.urls import path, include
from managementsystem import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('managementsystem.urls')), # redirects to log in page
    path('households/', include('households.urls')),
    path('operations/', include('operations.urls')),
    path('inventory/', include('inventory.urls')),
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
