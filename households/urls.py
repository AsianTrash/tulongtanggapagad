from django.urls import path

from . import views #from *this* directory import views.py

app_name = 'households'
urlpatterns =   [
    path('household_list/', views.IndexView.as_view(), name='index'),
    path('SMS/', views.SMSCreateView.as_view(), name='SMS'),
    path('registration/', views.RegistrationMenuView.as_view(), name='registration_menu'),
    path('import/', views.importFile, name='import_file'),
    path('registration/register/', views.RegistrationCreateView.as_view(), name='register'),
    path('registration/<int:pk>/update/', views.HouseholdUpdateView.as_view(), name='update'),
    path('household_list/<int:pk>/delete/', views.HouseholdDeleteView.as_view(), name='delete'),
    path('household_list/<int:pk>/reception_verification/', views.ReceptionUpdateView.as_view(), name='reception'),
]