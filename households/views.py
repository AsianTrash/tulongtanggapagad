from ast import Try
from datetime import datetime
from logging import exception
from django.http import request, HttpResponseRedirect
from django.shortcuts import render
from django.views import generic
from django.urls import reverse
from households.forms import *
from managementsystem.models import *
from managementsystem.forms import *
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.conf import settings  
from twilio.rest import Client
from django.contrib import messages
from django.shortcuts import redirect
from django.forms import *
import openpyxl
from extra_views import CreateWithInlinesView, UpdateWithInlinesView, InlineFormSetFactory
from extra_views.generic import GenericInlineFormSetView

class IndexView(generic.ListView):         
    template_name = 'households/index.html'
    model = Household

class RegistrationMenuView(generic.ListView):
    template_name = 'households/registration_menu.html'
    model = Household
    paginate_by = 5
    
    def get_context_data(self, **kwargs):
        context = super(RegistrationMenuView, self).get_context_data(**kwargs)
        context['new_households'] = Household.objects.all().order_by('-id')[:20]
        return context

class SMSCreateView(CreateView):
    template_name = 'households/sms.html'
    model = SMSMessage
    fields = ['message_title', 'message_content']
    
    def get_success_url(self):

        # test code. however unable to proceed testing with PH number due to trial account limitations.
        message_to_broadcast = ("message")
        client = Client("AC9d1a45e08480c7a61e5851636f7235c2", "4f121bbb233d0f88bdba37066efbe06b")

        client.messages.create(to="+639760125144", from_="+17407614481", body=message_to_broadcast) 

        return reverse('households:SMS')

    def get_context_data(self, **kwargs): #displaying messages
        # Call the base implementation first to get a context
        context = super(SMSCreateView, self).get_context_data(**kwargs)
        context['messages'] = SMSMessage.objects.all()
        return context

class ReceptionUpdateView(UpdateView):
    model = Household
    fields = ['reception_proof', 'has_received_assistance']
    template_name = 'households/reception.html'
    
    def get_success_url(self):
        message_to_broadcast = ("Relief package received! Date: " + str(datetime.today))
        client = Client("AC9d1a45e08480c7a61e5851636f7235c2", "4f121bbb233d0f88bdba37066efbe06b")
        client.messages.create(to="+639760125144", from_="+17407614481", body=message_to_broadcast) 

        return reverse('households:index')

class MembersInline(InlineFormSetFactory):
    model = Member
    extra = 0
    
    fields = ['firstname',
              'middlename',
              'lastname',
              'occupation',
              'gender',
              'birthday',]

    # configures inline fk model in the form
    factory_kwargs = {'extra': 1, 'max_num': 5,
                        'can_order': False, 'can_delete': True,}
                        
class RegistrationCreateView(CreateWithInlinesView):
    template_name = 'households/registration.html'
    model = Household
    inlines = [MembersInline]
    fields = ['house_number', 
              'street',
              'subdivision',
              'zone',
              'barangay',
              'household_head_firstname',
              'household_head_middlename',
              'household_head_lastname',
              'monthly_income',
              'contact_number',]
              
    def get_success_url(self):
        return reverse('households:registration_menu')

class HouseholdUpdateView(UpdateWithInlinesView):
    template_name = 'households/update_household.html'
    model = Household
    inlines = [MembersInline]
    fields = ['house_number', 
              'street',
              'subdivision',
              'zone',
              'barangay',
              'household_head_firstname',
              'household_head_middlename',
              'household_head_lastname',
              'monthly_income',
              'contact_number',]
              
    def get_success_url(self):
        return reverse('households:registration_menu')


class HouseholdDeleteView(DeleteView):
    template_name = 'households/delete_household.html'
    model = Household
              
    def get_success_url(self):
        return reverse('households:registration_menu')

def importFile(request):
    if "GET" == request.method:
        return reverse('households:registration_menu')
    elif request.method == "POST":
        excel_file = request.FILES["excel_file"]

        # you may put validations here to check extension or file size

        wb = openpyxl.load_workbook(excel_file)

        # getting a particular sheet by name out of many sheets
        worksheet = wb.active
        print(worksheet)

        excel_data = list()
        # iterating over the rows and
        # getting value from each cell in row

        i = 0
        for row in worksheet.iter_rows():
            row_data = list()
    
            for cell in row:
                row_data.append(str(cell.value))
            excel_data.append(row_data)
            if i > 0:
                Household.objects.update_or_create(
                    house_number = row_data[0],
                    street = row_data[1],
                    subdivision = row_data[2],
                    zone = row_data[3],
                    barangay = row_data[4],
                    city = row_data[5],
                    province = row_data[6],
                    household_head_firstname = row_data[7],
                    household_head_middlename = row_data[8],
                    household_head_lastname = row_data[9],
                    monthly_income = row_data[10],
                    contact_number = row_data[11],
                    number_of_household_members = row_data[12],
                )
            i += 1		
        return HttpResponseRedirect(reverse("households:registration_menu"))