from django.contrib import admin
from managementsystem.models import *
from django.conf.urls import url
from import_export import resources
from import_export.admin import ImportExportModelAdmin

class MembersInline(admin.StackedInline):
    model = Member
    extra = 0
    
    fieldsets = [  
        (None, {'fields': ['firstname']}),
        (None, {'fields': ['middlename']}),
        (None, {'fields': ['lastname']}),
        (None, {'fields': ['occupation']}),
        (None, {'fields': ['gender']}),
        (None, {'fields': ['birthday']}),
    ]

# class FamilyInline(admin.TabularInline):
#     model = Family
#     extra = 1

class HouseholdResource(resources.ModelResource):
    
    class Meta:
        model = Household
        fields = ('house_number', 'street', 'subdivision', 'zone', 'barangay', 'city', 'province',
                 'household_head_firstname', 'household_head_middlename','household_head_lastname',
                 'monthly_income', 'contact_number', 'number_of_household_members')
        export_order = ('house_number', 'street', 'subdivision', 'zone', 'barangay', 'city', 'province',
                 'household_head_firstname', 'household_head_middlename','household_head_lastname',
                 'monthly_income', 'contact_number', 'number_of_household_members')
        exclude = ('id',)
        import_id_fields = ('house_number', 'street', 'subdivision', 'zone', 'barangay', 'city', 'province',
                 'household_head_firstname', 'household_head_middlename','household_head_lastname',
                 'monthly_income', 'contact_number', 'number_of_household_members')

class HouseholdAdmin(ImportExportModelAdmin):
    fieldsets = [
        (None, {'fields': ['house_number']}),
        (None, {'fields': ['street']}),
        (None, {'fields': ['subdivision']}),
        (None, {'fields': ['zone']}),
        (None, {'fields': ['barangay']}),
        (None, {'fields': ['city']}),
        (None, {'fields': ['province']}),
        (None, {'fields': ['household_head_firstname']}),
        (None, {'fields': ['household_head_middlename']}),
        (None, {'fields': ['household_head_lastname']}),
        (None, {'fields': ['monthly_income']}),
        (None, {'fields': ['contact_number']}),
        (None, {'fields': ['number_of_household_members']}),
    ]
    inlines = [MembersInline]
    search_fields = ['house_number']
    list_display = ('house_number', 'household_head', 'address', 
                    'number_of_members', 'monthly_income', 'contact_number', 'has_received_assistance')
    
    readonly_fields = ['reception_proof', 'has_received_assistance', 'city', 'province']
    
    resource_class = HouseholdResource

class SMSMessageAdmin(admin.ModelAdmin): 
    list_display = ('message_title', 'message_content', 'date_sent')
    readonly_fields = ['message_title', 'message_content', 'date_sent']

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

admin.site.register(Household, HouseholdAdmin)
admin.site.register(SMSMessage, SMSMessageAdmin)