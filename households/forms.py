from multiprocessing.sharedctypes import Value
from extra_views import ModelFormSetView
from managementsystem.models import Household, Member
from django import forms
from phonenumber_field.formfields import PhoneNumberField
from django.forms import ModelForm, modelformset_factory

MemberFormSet = modelformset_factory(
    Member, fields=('firstname',
              'middlename',
              'lastname',
              'occupation',
              'gender',
              'birthday',), extra=1
)

class MemberForm(ModelForm):
    class Meta:
      model = Member
      fields = ['firstname',
              'middlename',
              'lastname',
              'occupation',
              'gender',
              'birthday',]