# This code reads the dependencies.deps file where all the dependencies
# used in this project are listed and runs an install/update command on them.

import subprocess
import sys

def install(package):
    subprocess.call([sys.executable, "-m", "pip", "install", package])

try:
    with open('dependencies.deps') as f:
        packages = f.readlines()
        packages = [p.strip() for p in packages]
        for pck in packages:
            if pck[0] != '#':
                install(pck)
except Exception as e:
    print('dependecies file not found!')
