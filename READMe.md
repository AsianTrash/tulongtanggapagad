# NOTES

* In managementsystem/models.py, change the value of "cred" to the current path of your firebaseadmin .json file. (.json file is in google drive).

* Run virtual environment while working on the code "smspy3/scripts/activate"

# NOTES ON CUSTOM FILES :

* Enter command "py rundeps.py" to install dependencies used in this project. Check the dependencies.deps file for any newly added dependencies.

* If auto-installation fails, run the commands manually: pip install -m "[dependency name]".

# SQLite3 Notes :

* To access db file (db.sqlite3) UI, download DB Browser here:
    https://sqlitebrowser.org/dl/

# TWILIO SMS API notes :

* Documentation : 
    https://www.twilio.com/blog/broadcast-sms-text-messages-python-3-django

* Dependencies :
    python3 -m venv smspy3 (run on console while on project main directory. /tulongtanggapagad/)
    smspy3/scripts/activate
    pip install twilio==6.32.0

* Run "py rundeps.py" after setting up virtual environment

# DJANGO IMPORT-EXPORT API Setup :

* Documentation :
    https://django-import-export.readthedocs.io/en/latest/installation.html

# MISC NOTES :

* test admin credentials : 
    - username : admin
    - email    : admin@sample.com
    - password : adminpassword