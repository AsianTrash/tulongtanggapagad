from django import forms
from .models import *
 
 
# creating a form
class SMSMessageForm(forms.ModelForm):
    
    message_title = forms.CharField(max_length=50)
    message_content = forms.Textarea()

    # create meta class
    class Meta:
        # specify model to be used
        model = SMSMessage
 
        # specify fields to be used
        fields = [
            "message_title",
            "message_content",
        ]