from django.contrib import admin
from django.db import models
from django.db.models.fields import Field
from .models import *
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.conf.urls import url
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export.fields import Field

admin.site.site_header = 'TulongTanggapAgad | admin'
admin.site.site_title = 'TulongTanggapAgad | admin'
admin.site.index_title = 'TulongTanggapAgad administration'
admin.empty_value_display = '**Empty**'

class BarangayInline(admin.StackedInline):
    model = Barangay
    can_delete = False
    verbose_name_plural = 'Barangays'
    readonly_fields = ['municipality']

class UserAdmin(BaseUserAdmin):
    inlines = (BarangayInline,)

class BarangayResource(resources.ModelResource):
    class Meta:
        model = Barangay
        fields = ('name', 'municipality')
        export_order = ('municipality', 'name')
        # skip_unchanged = True
        # report_skipped = True
        # exclude = ('id',)
        import_id_fields = ('municipality', 'name')

class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'pub_date')
    def has_change_permission(self, request, obj=None):
        return False

class BarangayAdmin(ImportExportModelAdmin):
    model = Barangay
    fieldsets = [(None, {'fields': ['name']}),]
    list_display = ('name', 'municipality', 'number_of_households', 'number_of_received_assistance')
    resource_class = BarangayResource

admin.site.register(News, NewsAdmin)
admin.site.register(Barangay, BarangayAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)