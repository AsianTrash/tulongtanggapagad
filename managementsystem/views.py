from django.shortcuts import render, get_object_or_404, reverse, redirect
from django.http import HttpResponseRedirect
from django.views import generic
from .models import Household, News, RiskAssessment, RiskAssessmentInfo
from django.contrib.auth.models import User
from django import forms
from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.utils import timezone
from django.views import generic

def login_request(request):
	if request.method == "POST":
		form = AuthenticationForm(data=request.POST)
		
		if form.is_valid():
			return redirect("managementsystem:main")
	else:
		form = AuthenticationForm()
	return render(request=request, template_name="managementsystem/login.html", context={"login_form":form})
    
def mainpage(request):
	pdra = RiskAssessment.objects.order_by('-assessment_date')[:1]
	assessmentinfo = RiskAssessmentInfo.objects.filter(assessment_form=pdra)
	context = {'riskassessment': pdra, 'riskassessmentinfo': assessmentinfo}
	return render(request, 'managementsystem/mainpage.html', context)

class UpdatesView(generic.ListView):
	template_name = 'managementsystem/updates_history.html'
	model = RiskAssessment
	paginate_by = 5

def unauthorized_access(request):
    return render(request, 'managementsystem/unauthorized.html')

def admin(request):
    return HttpResponseRedirect(reverse('admin:index'))
