# Generated by Django 3.2.8 on 2021-11-30 14:42

import django.core.files.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('managementsystem', '0024_alter_household_house_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='household',
            name='eligibility_proof',
            field=models.ImageField(null=True, upload_to=django.core.files.storage.FileSystemStorage(location='/media/photos/eligibility_proof')),
        ),
        migrations.AlterField(
            model_name='household',
            name='reception_proof',
            field=models.ImageField(null=True, upload_to=django.core.files.storage.FileSystemStorage(location='/media/photos/reception_proof')),
        ),
    ]
