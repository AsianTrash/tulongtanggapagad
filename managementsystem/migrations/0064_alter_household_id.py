# Generated by Django 3.2.8 on 2022-05-25 19:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('managementsystem', '0063_auto_20220526_0257'),
    ]

    operations = [
        migrations.AlterField(
            model_name='household',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
