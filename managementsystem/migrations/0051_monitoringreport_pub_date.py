# Generated by Django 3.2.8 on 2022-05-16 06:26

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('managementsystem', '0050_monitoringreport_trackingreport'),
    ]

    operations = [
        migrations.AddField(
            model_name='monitoringreport',
            name='pub_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
