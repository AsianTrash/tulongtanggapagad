# Generated by Django 3.2.8 on 2022-05-22 17:42

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('managementsystem', '0057_auto_20220523_0035'),
    ]

    operations = [
        migrations.CreateModel(
            name='DILGAdvisory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(default='', max_length=500)),
                ('date', models.DateField()),
                ('advisory_content', ckeditor.fields.RichTextField()),
            ],
            options={
                'verbose_name_plural': 'DILG Advisories',
            },
        ),
    ]
