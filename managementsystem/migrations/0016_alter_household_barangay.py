# Generated by Django 3.2.5 on 2021-09-16 07:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('managementsystem', '0015_remove_household_assigned_barangay'),
    ]

    operations = [
        migrations.AlterField(
            model_name='household',
            name='barangay',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='managementsystem.barangay', unique=True),
        ),
    ]
