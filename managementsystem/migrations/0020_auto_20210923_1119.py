# Generated by Django 3.2.5 on 2021-09-23 03:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('managementsystem', '0019_auto_20210923_1104'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='family',
            name='members',
        ),
        migrations.RemoveField(
            model_name='household',
            name='number_of_members',
        ),
        migrations.AddField(
            model_name='resident',
            name='family_group',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='managementsystem.family'),
        ),
    ]
