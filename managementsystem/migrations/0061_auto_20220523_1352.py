# Generated by Django 3.2.8 on 2022-05-23 05:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('managementsystem', '0060_auto_20220523_0236'),
    ]

    operations = [
        migrations.AlterField(
            model_name='budgetrequestinfo',
            name='required_documentation',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='riskassessmentinfo',
            name='required_action',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='supplyrequestinfo',
            name='item_description',
            field=models.TextField(default=''),
        ),
    ]
