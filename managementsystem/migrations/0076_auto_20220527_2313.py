# Generated by Django 3.2.8 on 2022-05-27 15:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('managementsystem', '0075_alter_riskassessmentinfo_response_strategy'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='member',
            name='family_group',
        ),
        migrations.DeleteModel(
            name='Family',
        ),
    ]
