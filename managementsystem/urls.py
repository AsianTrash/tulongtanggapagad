from django.urls import path

from . import views #from *this* directory import views.py

app_name = 'managementsystem'
urlpatterns =   [
    path('', views.login_request, name='index'),     # routes to login page
    path('main/', views.mainpage, name='main'),
    path('updates/', views.UpdatesView.as_view(), name='updates'),
    path('main/unauthorized/', views.unauthorized_access, name='unauthorized'),
    path('admin/', views.admin, name='admin'),
]