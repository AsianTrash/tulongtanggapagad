from email.policy import default
from turtle import update
from django.db import models 
from django.db.models.base import Model
from django.db.models.deletion import CASCADE
from django.db.models.fields.related import ForeignKey
from django.db.models import Count, Sum
from datetime import date, datetime
from django.core.files.storage import FileSystemStorage
from ckeditor.fields import RichTextField
from django.conf import settings
from django.contrib.auth.models import User
from django.forms import FloatField
from pytz import timezone
from django.core.mail import send_mail, EmailMessage
from django.utils.safestring import mark_safe
from phonenumber_field.modelfields import PhoneNumberField
from django.template.loader import get_template
from django.template import Context

eligibility_storage = FileSystemStorage(location='/media/photos/eligibility_proof')
reception_storage = FileSystemStorage(location='/media/photos/reception_proof')

class Barangay(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50, choices=settings.BARANGAYS_LIST)
    municipality = models.CharField(max_length=256, default='Pili')

    def number_of_households(self):
        households = Household.objects.filter(barangay = self)
        return households.count()
    
    def number_of_received_assistance(self):
        households = Household.objects.filter(barangay = self, has_received_assistance = True)
        return households.count()
    
    def completion_status(self):
        household = self.number_of_households()
        assisted = self.number_of_received_assistance()
        try:
            completion_status = assisted / household * 100
        except ZeroDivisionError:
            completion_status = 0.0

        return completion_status

    def __str__(self):
        return self.name

# Household < Family < Resident
class Household(models.Model):
    house_number = models.IntegerField(default=0)
    street = models.CharField(max_length=50, default='')
    subdivision = models.CharField(max_length=50, default='N/A', null=True)
    zone = models.IntegerField(default=0)
    barangay = models.CharField(max_length=50, choices=settings.BARANGAYS_LIST,null=True)
    city = models.CharField(max_length=50, default='Pili')
    province = models.CharField(max_length=50, default='Camarines Sur')

    household_head_firstname = models.CharField(max_length=50, default='')
    household_head_middlename = models.CharField(max_length=50, default='')
    household_head_lastname = models.CharField(max_length=50, default='')

    monthly_income = models.FloatField(default=0.00)
    contact_number = PhoneNumberField()

    number_of_household_members = models.IntegerField(default=1, null=True)

    reception_proof = models.ImageField(upload_to='reception_proof',  blank=True, null=True)

    has_received_assistance = models.BooleanField(default=False)

    def __str__(self):
        return str(self.house_number)
    
    def address(self):
        return self.street + ', ' + self.subdivision + ', Zone ' + str(self.zone) + ', ' + str(self.barangay) + ', ' + str(self.city) + ', ' + str(self.province)
    
    def household_head(self):
        return self.household_head_lastname + ', ' + self.household_head_firstname + ' ' + self.household_head_middlename[0].upper()

    def number_of_members(self):
        member = Member.objects.filter(household_group = self)
        if member.count() > 0:
            return member.count()
        elif member.count() == 0:
            return self.number_of_household_members

class Member(models.Model):
    household_group = models.ForeignKey(Household, on_delete=CASCADE, null=True)
    firstname = models.CharField(max_length=50, default='')
    middlename = models.CharField(max_length=50, default='')
    lastname = models.CharField(max_length=50, default='')
    occupation = models.CharField(max_length=50, default='')
    gender = models.CharField(max_length=15, choices=settings.GENDERS_LIST)
    birthday = models.DateField(default=date.today)

    def __str__(self):
        return self.firstname + ' ' + self.middlename[0].upper() + ' ' + self.lastname

class SMSMessage(models.Model):
    message_title = models.CharField(max_length=50, default='title')
    message_content = models.TextField(max_length=500, null=True)
    date_sent = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.message_title

# Relief Goods < Items < Donors

class Item(models.Model):
    name = models.CharField(max_length=50)
    quantity = models.IntegerField(default=1)
    fund_source = models.CharField(max_length=50, choices=[('donated', 'donated'),
                                                            ('local funds', 'local funds'),
                                                            ('national funds', 'national funds'),
                                                            ('international funds', 'international funds')]) #dropdown
    date_restocked = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Inventory'

    def get_number_of_stocks(self):
        items = Item.objects.filter(name = self)
        return items.count()

STATUS_CHOICES = (
    ('pending','PENDING'),
    ('approved', 'APPROVED'),
)

class ReliefPackage(models.Model):
    name = models.CharField(max_length=25, default='relief package')
    date_received = models.DateTimeField(auto_now_add=True)
    destination = models.CharField(max_length=256,  choices=settings.BARANGAYS_LIST, null=False)       # might create address model
    is_donated = models.BooleanField(default=False)
    relief_items = models.ManyToManyField(Item, default='')
    status = models.CharField(max_length=25, choices=STATUS_CHOICES, default='pending')
    date_of_approval = models.DateTimeField(null=True, blank=True)
    approval_instruction = RichTextField(null=True, blank=True)
    reception_status = models.BooleanField(default=False)
    
    def update (self, *args, **kwargs):
        self.date_of_approval = datetime.now
        super(ReliefPackage, self).update(*args, **kwargs)
    
    def __str__(self):
        return self.name

class Donor(models.Model):
    relief_package = models.ForeignKey(ReliefPackage, unique=True, on_delete=CASCADE)
    firstname = models.CharField(max_length=50, default='')
    middlename = models.CharField(max_length=50, default='')
    lastname = models.CharField(max_length=50, default='')
    email = models.EmailField(max_length=254, null=True)
    contact_number = PhoneNumberField()

    def __str__(self):
        return self.firstname + ' ' + self.middlename[0].upper() + ' ' + self.lastname

class News(models.Model):
    title = models.CharField(max_length=50, default='')
    message = RichTextField()
    pub_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'News'
    
    def __str__(self):
        return self.title

# Monitoring reports methods

def get_families_affected(municipality):
    families = Household.objects.filter(city = municipality)
    return families.count()

def get_persons_affected(municipality):
    families = Household.objects.filter(city = municipality)
    member_count = 0
    for persons in families:
        member_count += persons.number_of_members()

    return member_count
    
def get_families_served(municipality):
    families = Household.objects.filter(city = municipality).filter(has_received_assistance = True)
    return families.count()

def get_persons_served(municipality):
    families = Household.objects.filter(city = municipality).filter(has_received_assistance = True)
    member_count = 0
    for persons in families:
        member_count += persons.number_of_members()

    return member_count

# End Monitoring reports methods

class MonitoringReport(models.Model):
    municipality = models.CharField(max_length=50, default='Pili')
    household_affected = models.IntegerField(default=0)
    persons_affected = models.IntegerField(default=0)
    household_served = models.IntegerField(default=0)
    persons_served = models.IntegerField(default=0)
    remarks = models.CharField(max_length=50, null=True, default='')
    relief_pack_quantity = models.IntegerField(default=0)
    cost_spent = models.FloatField(default=0)
    pub_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Monitoring Reports (DSWD)' 
    
    def save(self, *args, **kwargs):
        self.household_affected = get_families_affected(self.municipality)
        self.persons_affected = get_persons_affected(self.municipality)
        self.household_served = get_families_served(self.municipality)
        self.persons_served = get_persons_served(self.municipality)
        self.relief_pack_quantity = ReliefPackage.objects.count()
        self.cost_spent = sum(Fund.objects.all().values_list('amount', flat=True))
        super(MonitoringReport, self).save(*args, **kwargs)
        
        template = get_template('D:/Capstone/web project/tulongtanggapagad/operations/templates/operations/monitoring_report.html')
        
        context = Context({'report': self}).flatten()
        content = template.render(context)

        mail = EmailMessage(
            'Monitoring Report ' + str(self.pub_date), 
            content, 
            settings.EMAIL_HOST_USER, 
            ['connivinggod7@gmail.com'],
            )
        mail.content_subtype = 'html'
        mail.send()

#Tracking reports methods

def get_number_of_registered_households(barangay):
        households = Household.objects.filter(barangay = barangay)
        return households.count()
    
def get_number_of_received_assistance(barangay):
    households = Household.objects.filter(barangay = barangay, has_received_assistance = True)
    return households.count()

def get_completion_status(barangay):
        household = barangay.number_of_households()
        assisted = barangay.number_of_received_assistance()
        try:
            completion_status = assisted / household * 100
        except ZeroDivisionError:
            completion_status = 0.0

        return completion_status

#End Tracking reports methods

class TrackingReport(models.Model):
    barangay = models.ForeignKey(Barangay, on_delete=CASCADE)
    registered_households = models.IntegerField(default=0)
    received_assistance = models.IntegerField(default=0)
    completion_rate = models.FloatField(default=0)
    remarks = models.CharField(max_length=50, null=True, default='')
    pub_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Tracking Reports (DSWD)' 
    
    def __str__(self):
        return str(self.barangay) + str(self.pub_date)

    def save(self, *args, **kwargs):

        self.registered_households = get_number_of_registered_households(self.barangay)
        self.received_assistance = get_number_of_received_assistance(self.barangay)
        self.completion_rate = get_completion_status(self.barangay)
        
        super(TrackingReport, self).save(*args, **kwargs)
        template = get_template('D:/Capstone/web project/tulongtanggapagad/operations/templates/operations/tracking_report.html')
        
        context = Context({'report': self}).flatten()
        content = template.render(context)

        mail = EmailMessage(
            'Tracking Report ' + str(self.pub_date), 
            content, 
            settings.EMAIL_HOST_USER, 
            ['connivinggod7@gmail.com'],
            )
        mail.content_subtype = 'html'
        mail.send()


class RiskAssessment(models.Model):
    workplace_location = models.CharField(max_length=500, default='', null=False)
    assessment_conductor = models.CharField(max_length=500, default='', null=False)
    assessment_date = models.DateField(null=False)
    reviewed_by = models.CharField(max_length=500, default='', null=False)

    class Meta:
        verbose_name_plural = 'Risk Assessment Forms' 

    def __str__(self):
        return self.workplace_location + ' ' + self.assessment_conductor + ' ' + str(self.assessment_date)
    
class RiskAssessmentInfo(models.Model):
    assessment_form = models.ForeignKey(RiskAssessment, on_delete=CASCADE)
    risk = models.TextField(default='', null=False)
    livelihood = models.CharField(max_length=500, choices=[('High', 'High'),
                                                            ('Medium', 'Medium'),
                                                            ('Low', 'Low')], null=False)
    hazard_level = models.CharField(max_length=500, choices=[('High', 'High'),
                                                            ('Significant', 'Significant'),
                                                            ('Moderate', 'Moderate'),
                                                            ('Low', 'Low')], null=False)
    actions_required = models.TextField(default='', null=False)
    action_taker = models.CharField(max_length=500, default='', null=False)
    action_date = models.DateField(null=False)
    response_strategy = models.CharField(max_length=500, choices=[('Avoid', 'Avoid'),
                                                            ('Transfer', 'Transfer'),
                                                            ('Mitigate', 'Mitigate'),
                                                            ('Accept', 'Accept')], null=False)
    evaluation_results = models.TextField(default='', null=False)
    
    class Meta:
        verbose_name_plural = 'Risk Assessment Information' 

class BudgetRequest(models.Model):
    requestor = models.CharField(max_length=500, default='', null=False)
    email = models.EmailField(max_length=254, null=False)
    date_requested = models.DateField(auto_now_add=True)

    request_period_start = models.DateField(null=False)
    request_period_end = models.DateField(null=False)
    
    households_file = models.FileField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Budget Request Forms'
    
    def __str__(self):
        return self.requestor + ' ' + str(self.date_requested)

    def save(self, *args, **kwargs):
        super(BudgetRequest, self).save(*args, **kwargs)
        template = get_template('D:/Capstone/web project/tulongtanggapagad/operations/templates/operations/budget_request.html')
        info = BudgetRequestInfo.objects.filter(budget_request_form = self)
        
        context = Context({'budgetrequest': self, 'infos': list(info)}).flatten()
        content = template.render(context)

        mail = EmailMessage(
            'Budget Request', 
            content, 
            settings.EMAIL_HOST_USER, 
            ['connivinggod7@gmail.com'],
            )
        mail.content_subtype = 'html'
        mail.send()

class BudgetRequestInfo(models.Model):
    budget_request_form = models.ForeignKey(BudgetRequest, on_delete=CASCADE)
    budget_category = models.CharField(max_length=500, default='', null=False)
    cost = models.FloatField(default=0, null=False) 
    required_documentation = models.TextField(default='', null=False)

    class Meta:
        verbose_name_plural = 'Budget Request Information'

    def __str__(self):
        return str(self.budget_request_form) + ' info'
    
    def save(self, *args, **kwargs):
        super(BudgetRequestInfo, self).save(*args, **kwargs)

class DILGAdvisory(models.Model):
    subject = models.CharField(max_length=500, default='', null=False)
    date = models.DateField(null=False)
    advisory_content = RichTextField()
    document = models.FileField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'DILG Advisories'

    def __str__(self):
        return self.subject + str(self.date)

    def advisory_content_html(self) :
        return mark_safe(self.advisory_content)

class SupplyRequest(models.Model):
    organization = models.CharField(max_length=50, default='', null=False)
    date_form_completed = models.DateField(null=False)
    date_needed_by = models.DateField(null=False)
    contact_person = models.CharField(max_length=50, default='', null=False)
    contact_number = PhoneNumberField()    
    
    class Meta:
        verbose_name_plural = 'Relief Goods Supply Request Forms'
    
    def __str__(self):
        return 'Supply Request ' + self.organization

    def save(self, *args, **kwargs):
        super(SupplyRequest, self).save(*args, **kwargs)
        
        template = get_template('D:/Capstone/web project/tulongtanggapagad/operations/templates/operations/supply_request.html')
        info = SupplyRequestInfo.objects.filter(supply_request_form = self)
        
        context = Context({'supplyrequest': self, 'infos': list(info)}).flatten()
        content = template.render(context)

        mail = EmailMessage(
            'Supply Request', 
            content, 
            settings.EMAIL_HOST_USER, 
            ['connivinggod7@gmail.com'],
            )
        mail.content_subtype = 'html'
        mail.send()

class SupplyRequestInfo(models.Model):
    supply_request_form = models.ForeignKey(SupplyRequest, on_delete=CASCADE)
    item_name = models.CharField(max_length=50, default='', null=False)
    item_description = models.TextField(default='', null=False)
    item_category = models.CharField(max_length=50, default='', null=False)
    quantity = models.IntegerField(default=0)
    cost = models.FloatField(default=0)
    sub_total = models.FloatField(default=0)

    class Meta:
        verbose_name_plural = 'Relief Goods Supply Request Information'
    
    def __str__(self):
        return str(self.supply_request_form) + ' info'
    
    def save(self, *args, **kwargs):
        self.sub_total = self.quantity * self.cost
        super(SupplyRequestInfo, self).save(*args, **kwargs)

class ApprovalDocument(models.Model):
    document_title = models.CharField(max_length=256, default='', null=False, blank=False)
    approval_file = models.FileField(null=False, blank=False)
    status = models.CharField(max_length=256, choices=STATUS_CHOICES, null=False, blank=False)
    approval_date = models.DateField(null=False, auto_now_add=True)
    
    class Meta:
        verbose_name_plural = 'Approval Documents (DSWD)'
    
    def __str__(self):
        return str(self.approval_file) + ' ' + str(self.approval_date)

class Fund(models.Model):
    amount = models.FloatField(default=0)
    source = models.CharField(max_length=50, choices=[('local funds', 'local funds'),
                                                            ('national funds', 'national funds'),
                                                            ('international funds', 'international funds')])
    date_released = models.DateField(null=False)
    date_received = models.DateField(null=False, auto_now_add=True)
    
    class Meta:
        verbose_name_plural = 'Funds'
    
    def __str__(self):
        return 'Fund ' + str(self.date_released) + str(self.date_received) + str(self.amount)

#accomplishment reports start

class AccomplishmentReport(models.Model):
    title = models.CharField(max_length=500, default='')
    introduction = models.TextField(default=settings.ACCOMPLISHMENT_REPORT_INTRO)
    report_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Accomplishment Reports'
    
    def __str__(self):
        return 'Accomplishment Report ' + str(self.report_date)
    
    def save(self, *args, **kwargs):
        super(AccomplishmentReport, self).save(*args, **kwargs)
        objective = ARObjective.objects.filter(report = self)
        highlight = ARHighlight.objects.filter(report = self)
        template = get_template('D:/Capstone/web project/tulongtanggapagad/operations/templates/operations/accomplishment_report.html')
        context = Context({'report': self, 'objectives': list(objective), 'highlights': list(highlight)}).flatten()
        content = template.render(context)

        mail = EmailMessage(
            'Accomplishment Report', 
            content, 
            settings.EMAIL_HOST_USER, 
            ['connivinggod7@gmail.com'],
            )
        mail.content_subtype = 'html'
        mail.send()

class ARObjective(models.Model):
    report = models.ForeignKey(AccomplishmentReport, on_delete=CASCADE)
    objective = models.CharField(max_length=500, default='', null=False)

    def __str__(self):
        return str(self.report) + ' objective'

class ARHighlight(models.Model):
    report = models.ForeignKey(AccomplishmentReport, on_delete=CASCADE)
    highlight = models.CharField(max_length=500, default='', null=False)

    def __str__(self):
        return str(self.report) + ' highlight'

#accomplishment reports end